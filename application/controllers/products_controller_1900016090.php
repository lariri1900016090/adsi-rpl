<?php 

class Products_controller_1900016090 extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->load->model('products_1900016090');
    }

    public function getProduct(){
        $data['getPoduct'] = $this->products_1900016090->getProducts();
        $this->load->view('client', $data);
    }

    public function getProductId($id_product){
        $data = $this->db ->get_where('product',['id_product'=> $id_product])->row_array();
        $this->load->view('client', $data);
    }

    public function postProduct(){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
         );
         $this->products_1900016090->addProduct($data);
    }

    public function deleteProduct( $id_product = NULL ){
        $data = array('id_product' => $id_product );
        $this->products_1900016090->deleteProduct($data);
    }

    public function putProduct( $id_barang = NULL ){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');

        $data = array(
            'id_product' => $id_product,
            'name' => $this->input->put('name'),
            'price' => $this->input->put('price'),
         );
         $this->products_1900016090->updateProduct($data);
    }
}


?>